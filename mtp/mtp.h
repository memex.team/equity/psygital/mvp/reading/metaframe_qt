#ifndef MTP_H
#define MTP_H

#include <QtNetwork/QNetworkAccessManager>
#include <QtNetwork/QNetworkReply>

class Mtp : public QObject{
  
  Q_OBJECT

public:
    QNetworkAccessManager *manager;
    QNetworkRequest request;
    QNetworkReply *reply;
    void get( QString url, QByteArray sessionToken);
    void post(QString url, QByteArray sessionToken, QByteArray Data);
    
private slots:
    void replyFinished(QNetworkReply *reply);
};

#endif // MTP_H 
