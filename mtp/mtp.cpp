// #######    ########    #######    ########    #######    #######    #######
// #      / / / /    License    \ \ \ \     
// #   Copyright © 2017-2021 Eugene Istomin
// #   Copyright © 2017-2021 Memex.Team
// #   Designed using Psygital®, Archestry®, Valuers.Stream®, Transfer.Games®
// #   and others Memex.Team concepts & products.    
// #   This code is covered by a Memex.Team Licensing policy (License.md)
// #######    ########    #######    ########    #######    #######    #######
// #      / / / /    Code Climate    \ \ \ \ 
// #    Language = ruby
// #    Indent = space; 2 chars;
// #######    ########    #######    ########    #######    #######    #######
#include "mtp.h"
#include "shell/shell.h"
#include "shell/shellutils.h"

#include "settings.h"
#include <QTextCodec>
#include <QString>
#include <QObject>
#include <QMessageBox>
#include <KMessageBox>
#include <QJsonDocument>
#include <QJsonObject>
#include <QDesktopServices>
#include <QDBusConnection>
#include <QDBusMessage>
#include <QDBusReply>

#include "mtp/json.hpp"

#define API_PATH "/api/text/metaframe"

void Mtp::get(QString apiAction, QByteArray sessionToken)
{   
    QUrl url(Okular::Settings::metaframeServerHost() + API_PATH + apiAction);
        
    manager = new QNetworkAccessManager();
    QNetworkRequest request;
    request.setUrl(url);
    request.setRawHeader("X-Session-Token", sessionToken);
    request.setRawHeader("Accept", "application/json");
    request.setHeader( QNetworkRequest::ContentTypeHeader, "application/json" );
    request.setHeader( QNetworkRequest::UserAgentHeader, "Metaframe QT v0.1" );

    reply = manager->get(request);

    QObject::connect (
    manager, SIGNAL(finished(QNetworkReply*)),
        this,   SLOT(replyFinished(QNetworkReply*))
    );
}

void Mtp::post(QString apiAction, QByteArray sessionToken, QByteArray Data)
{
    QUrl url(Okular::Settings::metaframeServerHost() + API_PATH + apiAction);
              
    manager = new QNetworkAccessManager();
    QNetworkRequest request;
    request.setUrl(url);
    request.setRawHeader("X-Session-Token", sessionToken);
    request.setRawHeader("Accept", "application/json");
    request.setHeader( QNetworkRequest::ContentTypeHeader, "application/json" );
    request.setHeader( QNetworkRequest::UserAgentHeader, "Metaframe QT v0.1" );

    reply = manager->post(request, Data);

    QObject::connect(
    manager, SIGNAL(finished(QNetworkReply*)),
        this,   SLOT(replyFinished(QNetworkReply*))
    );

}


void Mtp::replyFinished(QNetworkReply *reply)
{
    int status = reply->attribute( QNetworkRequest::HttpStatusCodeAttribute ).toInt();

    if ((reply->error() == QNetworkReply::NetworkSessionFailedError) || (reply->error() == QNetworkReply::TimeoutError) || (status == 0))
      KMessageBox::error( nullptr, "Seems we got a network error  ... can you please go online?", "Network problem");
    else {
      if ( status == 200 ) {
        
        QByteArray response_data = reply->readAll();
        QJsonDocument jsonResponse = QJsonDocument::fromJson(response_data);
        QJsonObject jsonObj = jsonResponse.object();

        qDebug() << status;
        qDebug() << jsonResponse;

        nlohmann::json rspJson = nlohmann::json::parse(response_data);
      
        if ( rspJson.contains("openUrl"))
          QDesktopServices::openUrl(QUrl(QString::fromStdString(rspJson["openUrl"]["url"].get<std::string>())));
        
//         else if ( rspJson.contains("openFile")) {     
//           const QString serializedOptions = "1:2:3:4:5:6:7";
//           Shell *shell = new Shell(serializedOptions);
//           const QString page = "5";
//           const QUrl url = ShellUtils::urlFromArg("/tmp/1.pdf", ShellUtils::qfileExistFunc(), page);
//           shell->openDocument(url, serializedOptions);
// 
//         }
        
        else if ( rspJson.contains("callDbus")) {
          QDBusMessage message = QDBusMessage::createMethodCall(QStringLiteral("org.freedesktop.ScreenSaver"), QStringLiteral("/ScreenSaver"), QStringLiteral("org.freedesktop.ScreenSaver"), QStringLiteral("Inhibit"));
          message << QCoreApplication::applicationName();
          message << "test";
          QDBusPendingReply<uint> reply = QDBusConnection::sessionBus().asyncCall(message);
        }          
        
        else if ( rspJson.contains("find"))
          QString action = "find";
        
        else if ( rspJson.contains("goToPage"))
          QString action = "goToPage";        
        
        else if ( rspJson.contains("information"))
            KMessageBox::information( nullptr, QString::fromStdString(rspJson["information"]["msg"].get<std::string>()), QString::fromStdString(rspJson["information"]["caption"].get<std::string>()));
        
        else if ( rspJson.contains("error"))
          KMessageBox::error( nullptr, QString::fromStdString(rspJson["error"]["msg"].get<std::string>()), QString::fromStdString(rspJson["error"]["caption"].get<std::string>()));
        
        else if ( rspJson.contains("question")) {
          QString response = "";
          KMessageBox::ButtonCode userAction = KMessageBox::questionYesNo(nullptr,
                  QString::fromStdString(rspJson["question"]["msg"].get<std::string>()),
                  QString::fromStdString(rspJson["question"]["caption"].get<std::string>())
                  );
          if (userAction == KMessageBox::Yes) {
            response = "yes";
          } else {
            response = "no";
          }
          
          nlohmann::json mtp;

          mtp["general"]["type"] = "feedback";
          mtp["general"]["layers"] = {"response"};
          mtp["general"]["timestamp"] = QDateTime::currentMSecsSinceEpoch();
          
          
          mtp["response"]["text"]   = response.toUtf8().constData();        
          mtp["response"]["uuid"]   = rspJson["question"]["uuid"];
          
          
          const std::string sessionToken = Okular::Settings::metaframeSessionToken().toUtf8().constData();  

          const QString apiAction = "/feedback";     
          //
          Mtp* mtpRemote = new Mtp();
          std::string json = mtp.dump();
          qDebug() << QString::fromStdString(json);
          
          mtpRemote->post(apiAction, QByteArray::fromStdString(sessionToken), QByteArray::fromStdString(json));
          // BUG - we need a non-200 handler
        }

      }
      else if ( status == 304 )
          KMessageBox::error( nullptr, "You got redirected .. seems like some network access restriction", "Access problem");
      else if ( status == 403 )
          KMessageBox::error( nullptr, "Seems like your have an invalid token. Please request a new token, or doublecheck the current.", "Access problem");
      else if ( status == 401 )
          KMessageBox::error( nullptr, "Seems like your have an invalid token. Please request a new token, or doublecheck the current.", "Access forbidden");
      else if ( status == 503 )
          KMessageBox::error( nullptr, "Serverside effect :) We do the best, let's try again", "Serverside problem");      
      else if ( status > 200 )
          KMessageBox::error( nullptr, "Seems you got a error.. Team is already fixing, keep calm ^)", "Remote problem");
    }

    reply->deleteLater();

}

