########    #######    ########    #######    ########    ########
##     / / / /    License    \ \ \ \ 
##    Copyleft culture, Copyright (C) is prohibited here
##    This work is licensed under a CC BY-SA 4.0
##    Creative Commons Attribution-ShareAlike 4.0 License
##    Refer to the http://creativecommons.org/licenses/by-sa/4.0/
########    #######    ########    #######    ########    ########
##    / / / /    Code Climate    \ \ \ \ 
##    Language = rpm spec
##    Indent = space;    4 chars;
########    #######    ########    #######    ########    ########

## // In4 \\
%global _obs_path %(ls -d %{_sourcedir}/*.obs)
%global _obs_helper     %(echo %{_obs_path}|sed -e "s/.*\\///")

%global _my_release_name    metaframe
%global _my_release_custname    %(echo %{_obs_helper}|sed -e "s/.*__CustName@//" -e "s/__.*//")
%global _my_release_version %(echo %{_obs_helper}|sed -e "s/.*__Ver@//" -e "s/__.*//")
%global _my_release_date    %(echo %{_obs_helper}|sed -e "s/.*__RelDate@//" -e "s/__.*//")
%global _my_release_tag     %(echo %{_obs_helper}|sed -e "s/.*__Hash@//" -e "s/__.*//")

#MANDATORY FIELDS
Version: %{_my_release_version}
Name:    %{_my_release_name}
Release: 1
## \\ In4 //

## SOURCES & PATCHES
#Source0: 
#Patch0:        
##

# BUG with lang
%bcond_with lang 

%define kf5_version 5.60.0
%{!?_kapp_version: %global _kapp_version %(echo %{version}| awk -F. '{print $1"."$2}')}

Summary:        Document Viewer
License:        GPL-2.0+
Group:          Productivity/Office/Other
Url:            http://www.kde.org
BuildRequires:  chmlib-devel
BuildRequires:  extra-cmake-modules
BuildRequires:  freetype2-devel
BuildRequires:  kf5-filesystem
BuildRequires:  libdjvulibre-devel
BuildRequires:  libepub-devel
BuildRequires:  libjpeg-devel
BuildRequires:  libmarkdown-devel
BuildRequires:  libpoppler-qt5-devel
BuildRequires:  libqca-qt5-devel
BuildRequires:  libspectre-devel
BuildRequires:  libtiff-devel
BuildRequires:  libzip-devel
BuildRequires:  zlib-devel
BuildRequires:  cmake(KF5Activities)
BuildRequires:  cmake(KF5Archive)
BuildRequires:  cmake(KF5Bookmarks)
BuildRequires:  cmake(KF5Completion)
BuildRequires:  cmake(KF5Config)
BuildRequires:  cmake(KF5ConfigWidgets)
BuildRequires:  cmake(KF5CoreAddons)
BuildRequires:  cmake(KF5Crash)
BuildRequires:  cmake(KF5DocTools)
BuildRequires:  cmake(KF5IconThemes)
BuildRequires:  cmake(KF5JS)
BuildRequires:  cmake(KF5KExiv2)
BuildRequires:  cmake(KF5KHtml)
BuildRequires:  cmake(KF5KIO)
BuildRequires:  cmake(KF5Parts)
BuildRequires:  cmake(KF5Pty)
BuildRequires:  cmake(KF5Purpose)
BuildRequires:  cmake(KF5ThreadWeaver)
BuildRequires:  cmake(KF5Wallet)
BuildRequires:  cmake(KF5WindowSystem)
BuildRequires:  cmake(Phonon4Qt5)
BuildRequires:  cmake(QMobipocket)
BuildRequires:  cmake(Qt5Core)
BuildRequires:  cmake(Qt5DBus)
BuildRequires:  cmake(Qt5PrintSupport)
BuildRequires:  cmake(Qt5Qml)
BuildRequires:  cmake(Qt5Quick)
BuildRequires:  cmake(Qt5Svg)
BuildRequires:  cmake(Qt5Test)
BuildRequires:  cmake(Qt5TextToSpeech)
BuildRequires:  cmake(Qt5Widgets)
Provides:       %{name} = %{version}
Obsoletes:      %{name} <= %{version}
Conflicts:      okular
Conflicts:      okular5

%if %{with lang}
Recommends:     %{name}-lang
%endif
BuildRoot:      %{_tmppath}/%{name}-%{version}-build

%description
Cognitive frames remapper

%package devel
Summary:        Development files for the Metaframe
Group:          Development/Libraries/KDE
Requires:       %{name} = %{version}
Requires:       cmake(KF5Config)
Requires:       cmake(KF5CoreAddons)
Requires:       cmake(KF5XmlGui)
Requires:       cmake(Qt5Core)
Requires:       cmake(Qt5PrintSupport)
Requires:       cmake(Qt5Widgets)
Obsoletes:      metaframe-devel < %{version}
Provides:       metaframe-devel = %{version}
Conflicts:      okular-devel
Conflicts:      okular5-devel

%description devel
Cognitive frames remapper

%if %{with lang}
%lang_package
%endif


%prep
## // In4 \\
cd %{_obs_path}
## \\ In4 //

#%patch0 -p1
#
#dos2unix doc/*.html

%build
## // In4 \\
mv %{_obs_path} %{_sourcedir}/../BUILD && cd %{_sourcedir}/../BUILD/*.obs
## \\ In4 //

%cmake_kf5 -d build -- -DBUILD_TESTING=ON
%make_jobs


%install
## // In4 \\
cd %{buildroot}/../../BUILD/*.obs
## \\ In4 //

%make_install -C build
 %if %{with lang}
   %find_lang %{name} --with-man --all-name
   %kf5_find_htmldocs
 %endif

rm -rfv %{buildroot}/%{_kf5_applicationsdir}/org.kde.mobile*
rm -rfv %{buildroot}/%{_kf5_applicationsdir}/org.kde.okular.kirigami*
rm -rfv %{buildroot}/%{_kf5_bindir}/okularkirigami


%post   -p /sbin/ldconfig
%postun -p /sbin/ldconfig

%files
%{_kf5_bindir}/metaframe
%{_kf5_debugdir}/okular.categories
%dir %{_kf5_htmldir}
%dir %{_kf5_htmldir}/en
%doc %lang(en) %{_kf5_htmldir}/en/*/
%doc %{_kf5_mandir}/man1/okular.*
%{_kf5_applicationsdir}/okularApplication_*.desktop
%{_kf5_applicationsdir}/org.kde.metaframe.desktop
%{_kf5_appstreamdir}/org.kde.okular-*.metainfo.xml
%{_kf5_appstreamdir}/org.kde.metaframe.*.xml
%{_kf5_configkcfgdir}/
%{_kf5_iconsdir}/hicolor/*/*/metaframe.*
%{_kf5_kxmlguidir}/
%{_kf5_libdir}/libOkular5Core.so.*
%dir %{_kf5_plugindir}/okular/
%dir %{_kf5_plugindir}/okular/generators/
%{_kf5_plugindir}/okularpart.so
%{_kf5_plugindir}/kf5/kio/kio_msits.so
%{_kf5_plugindir}/okular/generators/okularGenerator_*.so
%{_kf5_servicesdir}/okular*.desktop
%{_kf5_servicetypesdir}/okularGenerator.desktop
%{_kf5_sharedir}/kconf_update
%{_kf5_sharedir}/okular

%files devel
%{_kf5_cmakedir}/Okular5/
%{_kf5_libdir}/libOkular5Core.so
%{_kf5_prefix}/include/okular/

%if %{with lang}
%files lang -f %{name}.lang
%endif

%changelog 
